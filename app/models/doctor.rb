class Doctor < ActiveRecord::Base
  mount_uploader :image, ImageUploader

  has_many :writers

  has_many :medical_educations, inverse_of: :doctor
  accepts_nested_attributes_for :medical_educations, reject_if: :all_blank, allow_destroy: true

  has_many :professional_memberships, inverse_of: :doctor
  accepts_nested_attributes_for :professional_memberships, reject_if: :all_blank, allow_destroy: true

  has_many :courses, inverse_of: :doctor
  accepts_nested_attributes_for :courses, reject_if: :all_blank, allow_destroy: true
end

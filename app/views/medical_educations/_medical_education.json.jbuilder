json.extract! medical_education, :id, :title, :doctor_id, :created_at, :updated_at
json.url medical_education_url(medical_education, format: :json)

json.extract! vidio, :id, :title, :youtube_url, :server_url, :created_at, :updated_at
json.url vidio_url(vidio, format: :json)

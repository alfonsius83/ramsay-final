json.extract! service, :id, :title_id, :title_en, :content_id, :content_en, :slug, :created_at, :updated_at
json.url service_url(service, format: :json)

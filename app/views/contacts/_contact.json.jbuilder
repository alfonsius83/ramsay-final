json.extract! contact, :id, :email, :fullname, :address, :phone_number, :message, :city, :province, :created_at, :updated_at
json.url contact_url(contact, format: :json)

json.extract! doctor, :id, :name, :title_en, :title_id, :image, :created_at, :updated_at
json.url doctor_url(doctor, format: :json)

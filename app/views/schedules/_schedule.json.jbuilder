json.extract! schedule, :id, :day_en, :day_id, :start, :close, :doctor_id, :created_at, :updated_at
json.url schedule_url(schedule, format: :json)

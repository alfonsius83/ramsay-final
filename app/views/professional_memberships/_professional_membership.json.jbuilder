json.extract! professional_membership, :id, :title, :doctor_id, :created_at, :updated_at
json.url professional_membership_url(professional_membership, format: :json)

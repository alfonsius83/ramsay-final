json.extract! writer, :id, :name, :doctor_id, :doctor, :created_at, :updated_at
json.url writer_url(writer, format: :json)

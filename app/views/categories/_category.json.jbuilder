json.extract! category, :id, :title_en, :title_id, :created_at, :updated_at
json.url category_url(category, format: :json)

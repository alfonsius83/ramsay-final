json.extract! consultation, :id, :name, :email, :phone, :number, :subject, :content, :created_at, :updated_at
json.url consultation_url(consultation, format: :json)

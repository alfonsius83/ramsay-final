json.extract! article, :id, :title_en, :title_id, :content_id, :content_en, :category_id, :created_at, :updated_at
json.url article_url(article, format: :json)

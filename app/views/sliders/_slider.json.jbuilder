json.extract! slider, :id, :image, :title_en, :title_id, :caption_en, :caption_id, :url, :created_at, :updated_at
json.url slider_url(slider, format: :json)

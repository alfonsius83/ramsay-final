$(function () {
  $(document).scroll(function () {
    var $nav = $("#navbar");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
  });
});

$('.testi').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:1
        }
    }
})


$("#dc01").hover(function(){
  $("#sc01").show(300);
  $("#sc02").hide(300);
  $("#sc03").hide(300);
  $("#sc04").hide(300);
});
$("#dc02").hover(function(){
  $("#sc01").hide(300);
  $("#sc02").show(300);
  $("#sc03").hide(300);
  $("#sc04").hide(300);
});
$("#dc03").hover(function(){
  $("#sc01").hide(300);
  $("#sc02").hide(300);
  $("#sc03").show(300);
  $("#sc04").hide(300);
});
$("#dc04").hover(function(){
  $("#sc01").hide(300);
  $("#sc02").hide(300);
  $("#sc03").hide(300);
  $("#sc04").show(300);
});

$('.main-videos').owlCarousel({
  autoplay:true,
  autoplayTimeout:6000,
  autoplayHoverPause:false,
  items:1,
  nav:false,
  loop:true,
  animateOut: 'fadeOut',
  nav:true,
  URLhashListener:true,
  autoHeight:true
})

$('.thumb-videos').owlCarousel({
  loop:true,
  margin:10,
  autoplayTimeout:6000,
  nav:true,
  responsive:{
    0:{
      items:3
    },
    600:{
      items:3
    },
    1000:{
      nav:true,
      items:3
    }
  }
})

$('#feed').socialfeed({

  instagram:{
    accounts: ['@now_jakarta'],  //Array: Specify a list of accounts from which to pull posts
    limit: 8,                                    //Integer: max number of posts to load
    client_id: 'f771dcc38d634e349d30af3df6fc24a1',
    access_token: '1538402401.f771dcc.81ccbb099992479789505a4c6f2c154f'       //String: Instagram client id (optional if using access token)
  },

  // GENERAL SETTINGS
  length:400,                                     //Integer: For posts with text longer than this length, show an ellipsis.
  show_media:true,                                //Boolean: if false, doesn't display any post images
  media_min_width: 300,                           //Integer: Only get posts with images larger than this value
  //Integer: Number of seconds before social-feed will attempt to load new posts.

  template_html:
  '<div class="col-md-3 p-0"> \
  <a href="{{=it.link}}" target="_blank"> \
  <div class=""> \
  {{=it.attachment}} \
  </div> \
  </a> \
  </div>'                           //String: HTML used for each post. This overrides the 'template' filename option
});



// hide our element on page load


$('.anime').css('opacity', 0);


$('#service').waypoint(function() {
  $('#service').addClass('fadeIn');
}, { offset: '50%' });

$('#about-img').waypoint(function() {
  $('#about-img').addClass('fadeInLeft ');
}, { offset: '50%' });

$('#about-txt').waypoint(function() {
  $('#about-txt').addClass('fadeInRight');
}, { offset: '50%' });

$('#heading-doctor').waypoint(function() {
  $('#heading-doctor').addClass('fadeIn');
}, { offset: '50%' });

$('#dc01').waypoint(function() {
  $('#dc01').addClass('fadeInUp delay-1s');
}, { offset: '50%' });
$('#dc02').waypoint(function() {
  $('#dc02').addClass('fadeInUp delay-2s');
}, { offset: '50%' });
$('#dc02').waypoint(function() {
  $('#dc02').addClass('fadeInUp delay-3s');
}, { offset: '50%' });
$('#dc03').waypoint(function() {
  $('#dc03').addClass('fadeInUp delay-4s');
}, { offset: '50%' });
$('#dc04').waypoint(function() {
  $('#dc04').addClass('fadeInUp delay-5s');
}, { offset: '50%' });
$('#dc05').waypoint(function() {
  $('#dc05').addClass('fadeInUp delay-6s');
}, { offset: '50%' });

$('#srv-1').waypoint(function() {
  $('#srv-1').addClass('fadeInUp delay-1s');
}, { offset: '90%' });
$('#srv-2').waypoint(function() {
  $('#srv-2').addClass('fadeInUp delay-2s');
}, { offset: '90%' });
$('#srv-2').waypoint(function() {
  $('#srv-2').addClass('fadeInUp delay-3s');
}, { offset: '90%' });
$('#srv-3').waypoint(function() {
  $('#srv-3').addClass('fadeInUp delay-4s');
}, { offset: '90%' });
$('#srv-4').waypoint(function() {
  $('#srv-4').addClass('fadeInUp delay-5s');
}, { offset: '90%' });
$('#srv-5').waypoint(function() {
  $('#srv-5').addClass('fadeInUp delay-6s');
}, { offset: '90%' });

// var currentValue = 0;
// $('#belum').hide();
// function nikah(myRadio) {
//   if (myRadio.value == "sudah") {
//     $('#belum').hide();
//     $('#sudah').show();
//   } else {
//     $('#belum').show();
//         $('#sudah').show();
//   }
// }
// ('#insurancex').hide();
// function insurance(myRadio) {
//   if (myRadio.value == "self") {
//     $('#insurancex').hide();
//   } else {
//     $('#insurancex').show();
//
//   }
// }

$('.mhtc').matchHeight();

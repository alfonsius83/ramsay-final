// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require admin/modernizr
//= require admin/jquery
//= require jquery_ujs
//= require ckeditor/init
//= require admin/bootstrap
//= require admin/jquery.slimscroll.min
//= require admin/jquery.easing
//= require admin/jquery.appear
//= require admin/jquery.placeholder
//= require	admin/raphael.min.js
//= require	admin/morris.js
//= require admin/fastclick
//= require cocoon
//= require admin/main
//= require admin/offscreen
//= require select2

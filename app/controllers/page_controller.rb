class PageController < ApplicationController

  def index

  end

  def home
  end

  def about
    #code
  end

  def registration
    @registration = Registration.new
  end

  def consultation
    @consultation = Consultation.new
  end

  def article
    #code
  end

  def contact_us
    @contact = Contact.new
  end
end

class MedicalEducationsController < ApplicationController
  before_action :set_medical_education, only: [:show, :edit, :update, :destroy]

  # GET /medical_educations
  # GET /medical_educations.json
  def index
    @medical_educations = MedicalEducation.all
  end

  # GET /medical_educations/1
  # GET /medical_educations/1.json
  def show
  end

  # GET /medical_educations/new
  def new
    @medical_education = MedicalEducation.new
  end

  # GET /medical_educations/1/edit
  def edit
  end

  # POST /medical_educations
  # POST /medical_educations.json
  def create
    @medical_education = MedicalEducation.new(medical_education_params)

    respond_to do |format|
      if @medical_education.save
        format.html { redirect_to @medical_education, notice: 'Medical education was successfully created.' }
        format.json { render :show, status: :created, location: @medical_education }
      else
        format.html { render :new }
        format.json { render json: @medical_education.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /medical_educations/1
  # PATCH/PUT /medical_educations/1.json
  def update
    respond_to do |format|
      if @medical_education.update(medical_education_params)
        format.html { redirect_to @medical_education, notice: 'Medical education was successfully updated.' }
        format.json { render :show, status: :ok, location: @medical_education }
      else
        format.html { render :edit }
        format.json { render json: @medical_education.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /medical_educations/1
  # DELETE /medical_educations/1.json
  def destroy
    @medical_education.destroy
    respond_to do |format|
      format.html { redirect_to medical_educations_url, notice: 'Medical education was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medical_education
      @medical_education = MedicalEducation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medical_education_params
      params.require(:medical_education).permit(:title, :doctor_id)
    end
end

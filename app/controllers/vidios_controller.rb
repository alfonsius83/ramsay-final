class VidiosController < ApplicationController
  before_action :set_vidio, only: [:show, :edit, :update, :destroy]
  layout 'admin'
  # before_action :authenticate_user!

  # GET /vidios
  # GET /vidios.json
  def index
    @vidios = Vidio.all
  end

  # GET /vidios/1
  # GET /vidios/1.json
  def show
  end

  # GET /vidios/new
  def new
    @vidio = Vidio.new
  end

  # GET /vidios/1/edit
  def edit
  end

  # POST /vidios
  # POST /vidios.json
  def create
    @vidio = Vidio.new(vidio_params)

    respond_to do |format|
      if @vidio.save
        format.html { redirect_to @vidio, notice: 'Vidio was successfully created.' }
        format.json { render :show, status: :created, location: @vidio }
      else
        format.html { render :new }
        format.json { render json: @vidio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vidios/1
  # PATCH/PUT /vidios/1.json
  def update
    respond_to do |format|
      if @vidio.update(vidio_params)
        format.html { redirect_to @vidio, notice: 'Vidio was successfully updated.' }
        format.json { render :show, status: :ok, location: @vidio }
      else
        format.html { render :edit }
        format.json { render json: @vidio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vidios/1
  # DELETE /vidios/1.json
  def destroy
    @vidio.destroy
    respond_to do |format|
      format.html { redirect_to vidios_url, notice: 'Vidio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vidio
      @vidio = Vidio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vidio_params
      params.require(:vidio).permit(:title, :youtube_url, :server_url)
    end
end

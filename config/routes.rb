Rails.application.routes.draw do
  resources :services
  resources :contacts
  resources :registrations
  resources :courses
  resources :courses
  resources :writers
  resources :writers
  resources :writers
  resources :professional_memberships
  resources :medical_educations
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users
  resources :registrations
  resources :users
  resources :consultations
  resources :vidios
  resources :categories
  resources :articles
  resources :schedules
  resources :doctors
  resources :sliders
  root 'page#index'

  get 'index' =>'page#index' , :as => :home
  get 'about' =>'page#about' , :as => :about
  get 'registration' =>'page#registration' , :as => :registration_page
  get 'consultation' =>'page#consultation' , :as => :consultation_page

  get 'dashboard' =>'dashboard#index', :as => :dashboard
  get 'dr-sapto-adji-h-sp-ot' => 'page#doctor', :as => :single_doctor
  get 'cedera-bahu-yang-sering-terjadi' => 'page#article', :as => :single_article
  get 'chronic-shoulder-instability' => 'page#article', :as => :second_article
  get 'latest-article' => 'page#latest', :as => :latest
  get 'contact-us' => 'page#contact_us', :as => :contact_us


end

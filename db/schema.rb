# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190115125036) do

  create_table "articles", force: :cascade do |t|
    t.string   "title_en"
    t.string   "title_id"
    t.string   "content_id"
    t.string   "content_en"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "image"
    t.integer  "writer_id"
  end

  add_index "articles", ["category_id"], name: "index_articles_on_category_id"
  add_index "articles", ["writer_id"], name: "index_articles_on_writer_id"

  create_table "categories", force: :cascade do |t|
    t.string   "title_en"
    t.string   "title_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"

  create_table "consultations", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "number"
    t.string   "subject"
    t.string   "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "email"
    t.string   "fullname"
    t.string   "address"
    t.string   "phone_number"
    t.text     "message"
    t.string   "city"
    t.string   "province"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "courses", force: :cascade do |t|
    t.string   "title"
    t.integer  "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "courses", ["doctor_id"], name: "index_courses_on_doctor_id"

  create_table "doctors", force: :cascade do |t|
    t.string   "name"
    t.string   "title_en"
    t.string   "title_id"
    t.string   "image"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "speciality"
    t.string   "fb"
    t.string   "twitter"
    t.string   "instagram"
    t.string   "youtube"
    t.text     "desc"
    t.text     "desc_en"
    t.text     "mini_desc"
    t.text     "mini_desc_en"
  end

  create_table "medical_educations", force: :cascade do |t|
    t.string   "title"
    t.integer  "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "medical_educations", ["doctor_id"], name: "index_medical_educations_on_doctor_id"

  create_table "professional_memberships", force: :cascade do |t|
    t.string   "title"
    t.integer  "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "professional_memberships", ["doctor_id"], name: "index_professional_memberships_on_doctor_id"

  create_table "registrations", force: :cascade do |t|
    t.string   "name"
    t.string   "birth_date"
    t.string   "phonenumber"
    t.string   "email"
    t.string   "registerd"
    t.string   "id_type"
    t.string   "id_number"
    t.boolean  "merried"
    t.string   "gender"
    t.string   "address"
    t.string   "city"
    t.string   "province"
    t.string   "pos_code"
    t.string   "payment_method"
    t.string   "insurance_type"
    t.string   "service"
    t.string   "coming_date"
    t.string   "message"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.string   "day_en"
    t.string   "day_id"
    t.string   "start"
    t.string   "close"
    t.integer  "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "schedules", ["doctor_id"], name: "index_schedules_on_doctor_id"

  create_table "sliders", force: :cascade do |t|
    t.string   "image"
    t.string   "title_en"
    t.string   "title_id"
    t.string   "caption_en"
    t.string   "caption_id"
    t.string   "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "role"
    t.string   "username"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "vidios", force: :cascade do |t|
    t.string   "title"
    t.string   "youtube_url"
    t.string   "server_url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "writers", force: :cascade do |t|
    t.string   "name"
    t.integer  "doctor_id"
    t.boolean  "main"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "writers", ["doctor_id"], name: "index_writers_on_doctor_id"

end

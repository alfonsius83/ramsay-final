class CreateSliders < ActiveRecord::Migration
  def change
    create_table :sliders do |t|
      t.string :image
      t.string :title_en
      t.string :title_id
      t.string :caption_en
      t.string :caption_id
      t.string :url

      t.timestamps null: false
    end
  end
end

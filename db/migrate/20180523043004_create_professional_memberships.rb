class CreateProfessionalMemberships < ActiveRecord::Migration
  def change
    create_table :professional_memberships do |t|
      t.string :title
      t.references :doctor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

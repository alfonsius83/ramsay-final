class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title_en
      t.string :title_id
      t.string :content_id
      t.string :content_en
      t.references :category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class CreateMedicalEducations < ActiveRecord::Migration
  def change
    create_table :medical_educations do |t|
      t.string :title
      t.references :doctor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

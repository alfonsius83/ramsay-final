class CreateVidios < ActiveRecord::Migration
  def change
    create_table :vidios do |t|
      t.string :title
      t.string :youtube_url
      t.string :server_url

      t.timestamps null: false
    end
  end
end

class CreateRegistrations < ActiveRecord::Migration
  def change
    create_table :registrations do |t|
      t.string :name
      t.string :birth_date
      t.string :phonenumber
      t.string :email
      t.string :registerd
      t.string :id_type
      t.string :id_number
      t.boolean :merried
      t.string :gender
      t.string :address
      t.string :city
      t.string :province
      t.string :pos_code
      t.string :payment_method
      t.string :insurance_type
      t.string :service
      t.string :coming_date
      t.string :message

      t.timestamps null: false
    end
  end
end

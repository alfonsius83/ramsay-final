class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :title_id
      t.string :title_en
      t.text :content_id
      t.text :content_en
      t.string :slug

      t.timestamps null: false
    end
  end
end

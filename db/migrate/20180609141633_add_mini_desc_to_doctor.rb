class AddMiniDescToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :mini_desc, :text
    add_column :doctors, :mini_desc_en, :text
  end
end

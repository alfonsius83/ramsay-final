class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.string :day_en
      t.string :day_id
      t.string :start
      t.string :close
      t.references :doctor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

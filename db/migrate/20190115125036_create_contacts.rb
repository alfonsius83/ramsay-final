class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :email
      t.string :fullname
      t.string :address
      t.string :phone_number
      t.text :message
      t.string :city
      t.string :province

      t.timestamps null: false
    end
  end
end

class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title_en
      t.string :title_id

      t.timestamps null: false
    end
  end
end

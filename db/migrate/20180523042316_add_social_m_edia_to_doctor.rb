class AddSocialMEdiaToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :fb, :string
    add_column :doctors, :twitter, :string
    add_column :doctors, :instagram, :string
    add_column :doctors, :youtube, :string
  end
end

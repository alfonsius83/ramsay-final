class CreateWriters < ActiveRecord::Migration
  def change
    create_table :writers do |t|
      t.string :name
      t.references :doctor, index: true, foreign_key: true
      t.boolean :main

      t.timestamps null: false
    end
  end
end

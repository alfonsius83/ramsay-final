class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :name
      t.string :title_en
      t.string :title_id
      t.string :image

      t.timestamps null: false
    end
  end
end

class CreateConsultations < ActiveRecord::Migration
  def change
    create_table :consultations do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :number
      t.string :subject
      t.string :content

      t.timestamps null: false
    end
  end
end

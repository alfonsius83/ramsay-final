require 'test_helper'

class MedicalEducationsControllerTest < ActionController::TestCase
  setup do
    @medical_education = medical_educations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:medical_educations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create medical_education" do
    assert_difference('MedicalEducation.count') do
      post :create, medical_education: { doctor_id: @medical_education.doctor_id, title: @medical_education.title }
    end

    assert_redirected_to medical_education_path(assigns(:medical_education))
  end

  test "should show medical_education" do
    get :show, id: @medical_education
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @medical_education
    assert_response :success
  end

  test "should update medical_education" do
    patch :update, id: @medical_education, medical_education: { doctor_id: @medical_education.doctor_id, title: @medical_education.title }
    assert_redirected_to medical_education_path(assigns(:medical_education))
  end

  test "should destroy medical_education" do
    assert_difference('MedicalEducation.count', -1) do
      delete :destroy, id: @medical_education
    end

    assert_redirected_to medical_educations_path
  end
end

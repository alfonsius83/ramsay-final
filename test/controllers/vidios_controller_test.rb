require 'test_helper'

class VidiosControllerTest < ActionController::TestCase
  setup do
    @vidio = vidios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vidios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vidio" do
    assert_difference('Vidio.count') do
      post :create, vidio: { server_url: @vidio.server_url, title: @vidio.title, youtube_url: @vidio.youtube_url }
    end

    assert_redirected_to vidio_path(assigns(:vidio))
  end

  test "should show vidio" do
    get :show, id: @vidio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vidio
    assert_response :success
  end

  test "should update vidio" do
    patch :update, id: @vidio, vidio: { server_url: @vidio.server_url, title: @vidio.title, youtube_url: @vidio.youtube_url }
    assert_redirected_to vidio_path(assigns(:vidio))
  end

  test "should destroy vidio" do
    assert_difference('Vidio.count', -1) do
      delete :destroy, id: @vidio
    end

    assert_redirected_to vidios_path
  end
end
